# Think-In-JAVA
Java practice code. Created at March 3, 2018.<br/>
Tested with JRE version: 1.8.0.

### Code Library Tree
#### Basics
- Array<br/>
*Initiation, Array example.

- Composition<br/>
*Composition class example.

- EnumeratedType<br/>
*Enumerated Type example.

- InheritanceDesign<br/>
*A simple design sample.

- JavaDoc<br/>
*Random, JavaDoc example.

- Keyword-Final<br/>
*Keyword 'final' example.

- Loop<br/>
*For and While loop with label example.

- Modifier<br/>
*Class demo with dispose and key-word "modifier".

- Object<br/>
*Object, Constructor, Method example.

- ObjectStatement<br/>
*Object with statement 'this' example.

- Package&Import<br/>
*Package, import example.

- ReuseClass<br/>
*Reuse class demo with Composition, Inheritance, Delegation.


#### Security
- AES<br/>
*AES encrypt and decrypt example.

- HASH<br/>
*MD5, Salt, SHA-Series, PBKDF2 encrypt and verify example.  

//: Basics/Loop/TestRun.java
// For loops with labled break and continue.
public class TestRun {
    public static void main(String[] args) {
        int a = 0;
        outer: // Outside the main loop.
        for (; true ;) { // Infinite loop which is equal to 'while (ture)'.
            inner: // Inside the main loop.
            while (a < 10) {
                a ++;
                if (a == 2) {
                    System.out.println(a + " Continue.");
                    continue;
                }
                if (a == 3) {
                    System.out.println(a + " Continue Inner.");
                    continue inner;
                }
                if (a == 4) {
                    System.out.println(a + " Continue Outer");
                    continue outer;
                }
                if (a == 5) {
                    System.out.println(a + " Break Inner.");
                    break inner;
                }
                if (a == 6) {
                    System.out.println(a + " Break Outer.");
                    break outer;
                }
            }
        }
    }
}/* Output: (100% match)
2 Continue.
3 Continue Inner.
4 Continue Outer
5 Break Inner.
6 Break Outer.
*///:~

//: Basics/Composition/RunTest.java

public class RunTest {
    public static void main(String[] args) {
        System.out.println("Create Bank Account.");
        Bank myBank = new Bank("Reimu", 100);
        myBank.display();
        System.out.println("Deposit 200$");
        myBank.deposit(200);
        System.out.println("Try withdraw 200$ from this account.");
        myBank.withdraw(200);
        System.out.println("Try withdraw 300$ from this account.");
        myBank.withdraw(300);
    }
}

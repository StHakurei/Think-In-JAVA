//: Basics/Composition/Bank.java
/** Example code of composition class.
 * @author Murica_Reimu
*/

/** First class is used to create BankAccount */
class Account {
    private String name;
    private int balance;

    Account(String name) {
        this.name = name;
        balance = 0;
    }

    void change(int charge) {
        balance = balance + charge;
    }

    int getBalance() {
        return balance;
    }

    public String toString() {
        return ("Owner: " + name + " Balance: " + balance);
    }
}

/** Second class is used to operate BankAccount,
 * and user can only operates BankAccount through this class.
*/
public class Bank {
    private Account account;

    /** Default and only constructor.
     * @param name - Account Owner.
     * @param balance - Original Balance (Must >= 0).
    */
    public Bank(String name, int balance) {
        account = new Account(name);
        if (balance >= 0) {
            account.change(balance);
        }
        else {
            System.out.println("ERROR: Negative initial fund.");
        }
    }
    /** Deposit money to account. */
    public void deposit(int dollar) {
        if (dollar >= 0)
            account.change(dollar);
        else
            System.out.println("ERROR: Negative fund.");
    }
    /** Try withdraw money from account. */
    public void withdraw(int dollar) {
        if (dollar >= 0) {
            if (account.getBalance() >= dollar) {
                account.change(-(dollar));
            }
            else {
                System.out.println("ERROR: Insufficient fund.");
            }
        }
        else {
            System.out.println("ERROR: Negative fund.");
        }
    }
    /** Print information about account. */
    public void display() {
        System.out.println(account.toString());
    }
}

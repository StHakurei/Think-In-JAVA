//: Basics/AbstractClass/AnimalSound.java

/**
 * This sample code will performs the behavior of abstract class.
 * @author Murica_Reimu
*/
abstract class Animal {
    private String name;

    // abstract method left in uncomplete, it must be filled in drived class.
    public abstract void speak();
    public abstract void getSpecies();

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

/**
 * Use abstract class 'Animal' create drived class 'Dog' and 'Cat'.
*/
class Dog extends Animal {

    public Dog(String name) {
        setName(name);
    }

    // Override the abstract methods.
    public void speak() {
        System.out.println("Woof!");
    }

    public void getSpecies() {
        System.out.println("It's a Dog!");
    }
}

class Cat extends Animal {

    public Cat(String name) {
        setName(name);
    }

    // Override the abstract methods
    public void speak() {
        System.out.println("Meow!");
    }

    public void getSpecies() {
        System.out.println("It's a Cat!");
    }
}

public class AnimalSound {

    public static void main(String[] args) {
        /**
         * Animal a = new Animal();
         *
         * If you try to instantiate abstract class then you will get error
         * message from complier:
         *
         * "error: Animal is abstract; cannot be instantiated"
        */

        // Upcasting.
        Animal[] pets = {
            new Dog("David"),
            new Dog("John"),
            new Cat("Snow"),
            new Cat("Peter")
        };

        allSpeak(pets);
    }

    static void speaking(Animal object) {
        System.out.println(object.getName());
        object.getSpecies();
        object.speak();
    }

    static void allSpeak(Animal[] object_array) {
        for (Animal animal : object_array) {
            speaking(animal);
        }
    }
}/* Output: (100% match)
David
It's a Dog!
Woof!
John
It's a Dog!
Woof!
Snow
It's a Cat!
Meow!
Peter
It's a Cat!
Meow!
*///:~

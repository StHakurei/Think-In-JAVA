//: Basics/Modifier/ProtectSample.java

/** Java does provide key-word "protected" to control function access.
 * Key-word "public" means it can be accessed from global.
 * Key-word "private" means it can be only accessed from this class.
 * Key-word "protected" means it can be accessed from this class and anyone who inherits from this class or anyone else in the same package.
 * Without above key-words or no modifier means it can be accessed from same package, does not provide access from different package and public area.
 * @author Murica_Reimu
*/

class InnerClass {
    private String key;

    public InnerClass(String key) {
        this.key = key;
    }

    protected void resetKey(String key) {
        this.key = key;
    }

    public String readKey() {
        return key;
    }
}

class DriveredClass extends InnerClass {
    private String value;

    public DriveredClass(String key, String value) {
        super(key);
        this.value = value;
    }

    public void changeKey(String key) {
        super.resetKey(key);
        System.out.println("Reset key name as: " + super.readKey());
    }

    public void changeValue(String value) {
        this.value = value;
    }

    public void printKey() {
        System.out.println("Key: " + super.readKey() + " Value: " + this.value);
    }
}

public class ProtectSample {
    public static void main(String[] args) {
        DriveredClass object0 = new DriveredClass("Alpha", "0");
        object0.printKey();
        object0.changeKey("Omega");
        object0.changeValue("Infinite");
        object0.printKey();
    }
}/* Output: (100% match)
Key: Alpha Value: 0
Reset key name as: Omega
Key: Omega Value: Infinite
*///:~

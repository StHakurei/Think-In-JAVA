//: Basics/Modifier/DATASystem.java

/** Java does not have the C++ concept of a destructor,
 * a method that is called automatically when an object is destroyed.
 * So if you want to preform some clear up in a class,
 * you must explicitly write a method to do that,
 * and make sure that the client programmer knows that they must call this method.
 * @author Murica_Reimu.
*/

class Buffer {
    private String stored;

    Buffer (String buffer) {
        stored = buffer;
        System.out.println("Buffer - Constructor.");
    }

    String getMessage() {
        return stored;
    }

    void dispose() {
        System.out.println("Buffer dispose.");
    }
}

class StringBuffer extends Buffer {

    StringBuffer(String input) {
        super(input);
        System.out.println("StringBuffer - Constructor.");
    }

    String getMessage() {
        return super.getMessage();
    }

    void dispose() {
        System.out.println("Erasing StringBuffer.");
        super.dispose();
    }
}

public class DATASystem {

    public static void main(String[] args) {
        StringBuffer object = new StringBuffer("Test message.");
        try {
            object.toString();
            System.out.println("Buffered message: " + object.getMessage());
        }
        finally {
            object.dispose();
        }//The code in "finally" clause will be executed whatever the code in "try" clause is run successfully or not.
    }
}/* Output: (100% match)
Buffer - Constructor.
StringBuffer - Constructor.
Buffered message: Test message.
Buffer dispose.
Erasing StringBuffer.
*///:~

//: Basics/InheritanceDesign/Customer.java
/**
 * Inheritance Design example.
 * @author Murica_reimu
 * @version 1.0
 */
class Account {
    int prime;

    public Account() {
        prime = 0;
    }
}

class PrimeAccount extends Account {
    public PrimeAccount() {
        prime = 1;
    }
}

class OrdinaryAccount extends Account {
    public OrdinaryAccount() {
        prime = 2;
    }
}

class AccountOperator {
    private Account account;

    public AccountOperator() {
        account = new OrdinaryAccount();
    }

    public void upgrade() {
        account = new PrimeAccount();
    }

    public void downgrade() {
        account = new OrdinaryAccount();
    }

    public int getStatus() {
        return account.prime;
    }
}

public class Customer {
    public static void main(String[] args) {
        /**
         * This sample will create an account and upgrade it to prime,
         * then downgrade it to an ordinary status.
         */
        AccountOperator account = new AccountOperator();
        System.out.println("Customer Level: " + account.getStatus());
        account.upgrade();
        System.out.println("Customer Level: " + account.getStatus());
        account.downgrade();
        System.out.println("Customer Level: " + account.getStatus());
    }
}/* Output (100% match)
Customer Level: 2
Customer Level: 1
Customer Level: 2
*///:~

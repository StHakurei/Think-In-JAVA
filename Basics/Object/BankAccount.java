//: Basics/Object/BankAccount.java
import java.util.*;

/**
 * This example program will performs Java default initialization.
 * @author Murica_Reimu
 * @version 1.0
 *
*/
public class BankAccount {
    private String customerName;
    private char customerLevel;
    private int balance;
    /** Used to create an object instance without initialization. */
    public BankAccount(){};

    /** Used to create an object instance with initialization.
     * @param name customer's name.
     * @param level customer's level.
     * @param balance the money of this account.
    */
    public BankAccount(String name, char level, int balance) {
        customerName = name;
        customerLevel = level;
        this.balance = balance;
    }

    /** Output specific account detail which includes Owner, Customer Leve and the Balance in terminal.
    */
    public void showDetail() {
        System.out.print("Owner: " + customerName + "\n");
        System.out.print("Customer Level: " + customerLevel + "\n");
        System.out.print("Balance: " + balance + "$\n");
    }

    /** Return specific account detail which includes Owner, Customer Leve and the Balance with type String.
     * @return String - account's detail.
    */
    public String getDetail() {
        return ("Owner: " + customerName + "\n" +
                "Customer Level: " + customerLevel + "\n" +
                "Balance: " + balance + "$");
    }

    /** Used to check current status of the account.
     * @return boolean - this bank account is bankrupt.
    */
    public boolean isBankrupt() {
        return (balance < 0);
    }

    /** Used to try close current bank account.
     * @return int - Code 0(Successful), Code 1(Failed with negative balance), Code 2(Failed with positive balance)
    */
    public int close() {
        if (balance == 0) {
            System.out.println("Account is closed.");
        }
        else {
            System.out.println("This account cannot be closed at the moment.");
            if (balance < 0) {
                System.out.println("This customer needs to pay back money.");
                System.out.println("Billing - " + Math.abs(balance) + " $");
                return 1;
            }
            else if (balance > 0) {
                System.out.println("This account still contains fund.");
                System.out.println("Balance - " + balance);
                return 2;
            }
        }
        return 0;
    }
}

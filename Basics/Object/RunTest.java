//: Basics/Object/RunTest.java
public class RunTest {
    public static void main(String[] args) {
        BankAccount account1 = new BankAccount();
        System.out.println("Output account detail without initialization.");
        account1.showDetail();

        BankAccount account2 = new BankAccount("Peter", 'D', 200);
        System.out.println("Output account detail with initialization.");
        account2.showDetail();
    }
}/* Output: (100% match)
Output account detail without initialization.
Owner: null
Customer Level:
Balance: 0$
Output account detail with initialization.
Owner: Peter
Customer Level: D
Balance: 200$
*///:~

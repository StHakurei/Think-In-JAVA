//: Basics/Array/RandomArray.java
import java.util.*;
/** This example code will generate a random array (int)
 * @author Murica_Reimu
*/
public class RandomArray {
    Random generator = new Random();
    int[] result;

    /** Default constructor will generate a random array with length 10. */
    public RandomArray() {
        result = new int[10];
        for (int i = 0; i < 10; i++) {
            result[i] = generator.nextInt(100);
        }
    }

    /** This constructor will generate a random array in specified length.
     * @param size - Specify the length of this random array.
    */
    public RandomArray(int size) {
        result = new int[size];
        for (int i = 0; i < size; i++) {
            result[i] = generator.nextInt(100);
        }
    }

    /** Output content of the random array. */
    public void print() {
        System.out.println("Length: " + result.length);
        System.out.println(Arrays.toString(result));
    }

    public static void main(String[] args) {
        System.out.println("Generate a default random array.");
        RandomArray array_0 = new RandomArray();
        array_0.print();
        System.out.println("Generate a random array with specified length.");
        RandomArray array_1 = new RandomArray(5);
        array_1.print();
    }
}/* Output: (50% match)
Generate a default random array.
Length: 10
[9, 39, 23, 54, 72, 5, 21, 55, 10, 85]
Generate a random array with specified length.
Length: 5
[99, 10, 11, 50, 28]
*///:~

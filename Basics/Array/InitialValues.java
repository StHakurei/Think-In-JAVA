//: Basics/Array/InitialValues.java

/**
 * This example code will display default initialize value of variable on screen.
 * @author Murica_Reimu
*/
public class InitialValues {
    private boolean a;
    private char b;
    private byte c;
    private short d;
    private int e;
    private long f;
    private float g;
    private double h;
    private InitialValues reference;

    private void print() {
        System.out.println("Java default initialize value.");
        System.out.println("Boolean: " + a);
        System.out.println("Char: [" + b + "]");
        System.out.println("Byte: " + c);
        System.out.println("Short: " + d);
        System.out.println("Int: " + e);
        System.out.println("Long: " + f);
        System.out.println("Float: " + g);
        System.out.println("Double: " + h);
        System.out.println("Object Reference: " + reference);
    }

    public static void main(String[] args) {
        InitialValues object = new InitialValues();
        object.print();
    }
}/* Output: (100% match)
Java default initialize value.
Boolean: false
Char: [ ]
Byte: 0
Short: 0
Int: 0
Long: 0
Float: 0.0
Double: 0.0
Object Reference: null
*///:~

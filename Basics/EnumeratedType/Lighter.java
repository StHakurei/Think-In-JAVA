//: Basics/EnumeratedType/Lighter.java
/** Create a enumerated types for lighter.
 * @author Murica_Reimu
*/
public enum Lighter {
    ON, OFF, BRIGHTER, GLOOMY
}///:~

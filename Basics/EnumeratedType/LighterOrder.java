//: Basics/EnumeratedType/LighterOrder.java
/** Example code of useage of enumerated type.
 * @author Murica_Reimu
*/
public class LighterOrder {
    Lighter action;

    public LighterOrder(Lighter action) {
        this.action = action;
    }

    public void takeAction() {
        System.out.println("Take action on lighter:");
        switch (action) {
            case ON:
                System.out.println("Turn on lighter.");
                break;
            case BRIGHTER:
                System.out.println("Brighter lighter.");
                break;
            case GLOOMY:
                System.out.println("Dim lighter");
                break;
            case OFF:
                System.out.println("Turn off lighter");
                break;
            default:
                System.out.println("Lighter is not functional.");
        }
    }

    public static void main(String[] args) {
        LighterOrder lighter0 = new LighterOrder(Lighter.ON),
                     lighter1 = new LighterOrder(Lighter.OFF),
                     lighter2 = new LighterOrder(Lighter.BRIGHTER);
        lighter0.takeAction();
        lighter1.takeAction();
        lighter2.takeAction();
    }
}/* Output: (100% match)
Take action on lighter:
Turn on lighter.
Take action on lighter:
Turn off lighter
Take action on lighter:
Brighter lighter.
*///:~

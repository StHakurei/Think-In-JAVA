//: Basics/Interfaces/AnimalSound.java

/**
 * This sample code will performs the behavior of interface.
 * @author Murica_Reimu
*/

/**
 * An interface can be declared with keyword 'public'
 * but the interface name must be same as the file.
 * Otherwise, you get package access by default.
*/
interface Animal {
    /**
     * If you declare some valriable in interface,
     * it will be set in static and final.
    */
    String name = null;

    /**
     * Methods in an interface are automatically public,
     * and interface abstract methods cannot have body.
    */
    void speak();
    void getSpecies();
    void setName(String name);
    String getName();
}

class Dog implements Animal {
    String theName;

    public Dog(String name) {
        setName(name);
    }

    @Override
    public void speak() {
        System.out.println("Woof!");
    }

    @Override
    public void getSpecies() {
        System.out.println("It's a Dog!");
    }

    @Override
    public void setName(String name) {
        theName = name;
    }

    @Override
    public String getName() {
        return theName;
    }
}

class Cat implements Animal {
    String theName;

    public Cat(String name) {
        setName(name);
    }

    @Override
    public void speak() {
        System.out.println("Meow!");
    }

    @Override
    public void getSpecies() {
        System.out.println("It's a Cat!");
    }

    @Override
    public void setName(String name) {
        theName = name;
    }

    @Override
    public String getName() {
        return theName;
    }
}

public class AnimalSound {
    public static void main(String[] args) {
        Animal[] pets = {
            new Dog("David"),
            new Dog("John"),
            new Cat("Snow"),
            new Cat("Peter")
        };
        allSpeak(pets);
    }

    static void speaking(Animal object) {
        System.out.println(object.getName());
        object.getSpecies();
        object.speak();
    }

    static void allSpeak(Animal[] object_array) {
        for (Animal animal : object_array) {
            speaking(animal);
        }
    }
}/* Output: (100% match)
David
It's a Dog!
Woof!
John
It's a Dog!
Woof!
Snow
It's a Cat!
Meow!
Peter
It's a Cat!
Meow!
*///:~

//: Basics/JavaDoc/Mathematicals.java
import java.util.*;

/**
 * This example program will performs random number to terminal.
 * Run RunTest.java which located in same folder to check results.
 * @author Murica_Reimu
 * @version 1.0
*/
public class Mathematicals {
    private int randomInt;
    private float randomFloat;
    private Random rand;

    /** Use to create random object without initialize seed. */
    public Mathematicals() {
        rand = new Random();
    }

    /** Use to create random object with initialize seed.
     * @param seed give an initialize seed.
    */
    public Mathematicals(int seed) {
        rand = new Random(seed);
    }

    /** Get a random number with type int.
     * @return int - a random number.
    */
    public int getRandomInt() {
        int a, b, c;
        a = rand.nextInt(100) + 1;
        b = rand.nextInt(100) + 1;
        c = (a - b) / (a + b);
        return c;
    }

    /** Get a random number with type float.
     * @return float - a random number.
    */
    public float getRandomFloat() {
        float a, b, c;
        a = rand.nextFloat();
        b = rand.nextFloat();
        c = (a + b) / (a - b);
        return c;
    }
}

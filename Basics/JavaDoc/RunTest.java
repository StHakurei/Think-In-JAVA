//: Basics/JavaDoc/RunTest.java
public class RunTest {
    public static void main(String[] args) {
        System.out.println("Generate random number without seed.");
        Mathematicals noSeed = new Mathematicals();
        System.out.println("Integer: " + noSeed.getRandomInt());
        System.out.println("Float: " + noSeed.getRandomFloat());

        System.out.println("Generate random number with seed 37.");
        Mathematicals withSeed = new Mathematicals(37);
        System.out.println("Integer: " + withSeed.getRandomInt());
        System.out.println("Float: " + withSeed.getRandomFloat());
    }
}/* Output: (50% match)
Generate random number without seed.
Integer: 0
Float: -1.428494
Generate random number with seed 37.
Integer: 0
Float: 2.850172
*///:~

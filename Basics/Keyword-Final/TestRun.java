//: Basics/Keyword-Final/TestRun.java
/** This example code will performs how keyword 'final' works.
 * @author Murica_reimu
 * @version 1.0
*/
class Data {
    int value;
    public Data(int value) {
        this.value = value;
    }

    public void setData(int value) {
        this.value = value;
    }

    public int getData() {
        return this.value;
    }

    /**
     * Final method cannot be override by inherited class.
     * If you want to keep this method in no change,
     * declare it with keyword 'final'.
     */
    final public boolean isZero() {
        if (this.value == 0) {
            return true;
        }
        else {
            return false;
        }
    }
}

class BufferedData extends Data {

    public BufferedData(int value) {
        super(value);
    }

    @Override
    public void setData(int value) {
        super.setData(value);
        System.out.println("Setted new data.");
    }

    /**
     *    @Override
     *    public boolean isZero() {
     *        return true;
     *    }
     * You cannot override final method of based class.
     * If you try it you will get an error message like following:
     *
     *     error: isZero() in BufferedData cannot override isZero() in Data
     *     public boolean isZero() {
     *                    ^
     *     overridden method is final
     *     1 error
     */
}

public class TestRun {
    public static void main(String[] args) {
        /** Keyword 'final' makes value a constant.
         * Value is unchangable once value is given.
         * But you can refer it freely.
        */
        final String name = "Reimu";
        System.out.println(name);
        String nameByRefer = name;
        System.out.println(nameByRefer);
        /** You cannot modify a final variable.
         *
         *    name = "Hakurei";
         *
         * >> error: cannot assign a value to final variable name
        */

        /** In object, keyword 'final' makes the reference a constant.
         * Object itself can be modified.
         * But you cannot change it to point another object.
        */
        final Data sample = new Data(100);
        sample.setData(200);
        sample.value = 300;
        System.out.println(sample.getData());
        System.out.println("Is number is 0? " + sample.isZero());

        /** You cannot change it to point another object.
         *
         *    sample = new Data(99);
         *
         * >> error: cannot assign a value to final variable sample.
        */

        BufferedData buffer = new BufferedData(100);
        buffer.setData(50);
        buffer.value = 0;
        System.out.println(buffer.getData());
        System.out.println("Is number is 0? " + buffer.isZero());
    }
}

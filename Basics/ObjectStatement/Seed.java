//: Basic/ObjectStatement/Seed.java

/**
 * This example program will performs 'this' keyword with object.
 * @author Murica_Reimu
 * @version 1.0
*/
public class Seed {
    int seed;
    /** Default constructor will initialize seed with integer zero. */
    public Seed() {
        seed = 0;
    }
    /** This constructor will initialize seed with given argument(int).
     * @param input The starting number which you want.
    */
    public Seed(int input) {
        seed = input;
    }
    /** This method will make self-increment of variable seed and return Object Seed.
     * @return object Seed.
    */
    public Seed growth() {
        seed ++;
        return this;
    }
    /** Print current value of variable seed of current object. */
    public void show() {
        System.out.println(seed);
    }
}

//: Basics/ObjectStatement/TestRun.java
public class TestRun {
    public static void main(String[] args) {
        Seed seed1 = new Seed(),
             seed2 = new Seed(10);
        seed1.growth().growth().show();
        seed2.growth().growth().show();
    }
}/* Output: (100% match)
2
12
*///~

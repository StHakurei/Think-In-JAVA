//: Basics/Upcasting-And-Downcasting/RunTest.java
/**
 * This example code will performs what's Upcasting and Downcasting in Java.
 * @author Murica_reimu
 * @version 1.0
 */
 class Enegy {
     int baseCode = 0;

     void getType() {
         System.out.println("Type: Enegy.");
     }

     boolean isEnegy(int baseCode) {
         return this.baseCode == baseCode;
     }
 }

 class Substance extends Enegy {
     int baseCode = 1;
     int secretCode = 2;

     @Override
     void getType() {
         System.out.println("Type: Substance.");
     }

     boolean isSubstance(int baseCode) {
         return this.baseCode == baseCode;
     }
 }

 public class RunTest {

     public static void main(String[] args) {
         // Normal instantiation.
         System.out.println("\nEnegy enegy = new Enegy(); # Normal instantiation");
         Enegy enegy = new Enegy();
         enegy.getType();
         System.out.println("BaseCode: " + enegy.baseCode);
         System.out.println("It's Enegy? " + enegy.isEnegy(enegy.baseCode));

         System.out.println("\nSubstance substance = new Substance(); # Normal instantiation");
         Substance substance = new Substance();
         substance.getType();
         System.out.println("BaseCode: " + substance.baseCode);
         System.out.println("SecretCode: " + substance.secretCode);
         System.out.println("It's Enegy? " + substance.isEnegy(substance.baseCode));
         System.out.println("It's Substance? " + substance.isSubstance(substance.baseCode));

         // Upcasting.
         /**
          * When you check the output of this part of code, the method baseCode()
          * of these two instance give number 0, and if you try to output variable 'secretCode'
          * then you will get error message when compile.
          * Because of veriables decision happens at Compile Time, so always based class's variable
          * will be accessed.
          *
          * For example, following code will get error message when compile.
          *
          *    System.out.println(string_a.secretCode);
          *
          *     error: cannot find symbol
          *     System.out.println(string_a.secretCode);
          *                                ^
          *     symbol:   variable secretCode
          *     location: variable string_a of type Enegy
          *     1 error
          */
         System.out.println("\nEnegy string_a = new Substance(); # Upcasting");
         Enegy string_a = new Substance();
         string_a.getType();
         System.out.println("BaseCode: " + string_a.baseCode);
         System.out.println("It's Enegy? " + substance.isEnegy(string_a.baseCode));
         System.out.println("It's Substance? " + substance.isSubstance(string_a.baseCode));

         System.out.println("\nEnegy string_b = (Enegy) new Substance(); # Upcasting");
         Enegy string_b = (Enegy) new Substance();
         string_b.getType();
         System.out.println("BaseCode: " + string_b.baseCode);
         System.out.println("It's Enegy? " + substance.isEnegy(string_b.baseCode));
         System.out.println("It's Substance? " + substance.isSubstance(string_b.baseCode));

         // Downcasting.
         /**
          * 'Substance quark_a = new Enegy();'
          *
          * Dierct downcasting in Java is not possible, you will get compile time error.
          *
          *     error: incompatible types: Enegy cannot be converted to Substance
          *     Substance quark_a = new Enegy();
          *                         ^
          *     1 error
          *
          * Non-derict downcasting is supported in Java.
          * But this downcasting will not give Enegy object to Substance's reference variable.
          * This following code is almost equivalent to 'Substance quark_a = new Substance();'.
          */
          System.out.println("\nEnegy vibrate_string_c = new Substance();");
          System.out.println("Substance quark_a = (Substance) vibrate_string_c; # Downcasting");
          Enegy vibrate_string_c = new Substance();
          Substance quark_a = (Substance) vibrate_string_c;
          quark_a.getType();
          System.out.println("BaseCode: " + quark_a.baseCode);
          System.out.println("It's Enegy? " + substance.isEnegy(quark_a.baseCode));
          System.out.println("It's Substance? " + substance.isSubstance(quark_a.baseCode));
     }
 }/* Output: (100% match)
 Enegy enegy = new Enegy(); # Normal instantiation
Type: Enegy.
BaseCode: 0
It's Enegy? true

Substance substance = new Substance(); # Normal instantiation
Type: Substance.
BaseCode: 1
SecretCode: 2
It's Enegy? false
It's Substance? true

Enegy string_a = new Substance(); # Upcasting
Type: Substance.
BaseCode: 0
It's Enegy? true
It's Substance? false

Enegy string_b = (Enegy) new Substance(); # Upcasting
Type: Substance.
BaseCode: 0
It's Enegy? true
It's Substance? false

Enegy vibrate_string_c = new Substance();
Substance quark_a = (Substance) vibrate_string_c; # Downcasting
Type: Substance.
BaseCode: 1
It's Enegy? false
It's Substance? true
*///:~

//: Basics/Upcasting-And-Downcasting/otherSample/RunTest.java

/**
 * This example code will performs Upcasting and Downcasting with more detail.
 * @author Murica_reimu
 * @version 1.0
 */

class Creature {
    private String name = null;

    public Creature(String name) {
        this.name = name;
    }
}

class Animal extends Creature{

    public Animal(String name) {
        super(name);
    }

    public void breath() {
        System.out.println("I can breath!");
    }

    public void speak() {
        System.out.println("No word.");
    }
}

class Cat extends Animal {

    public Cat(String name) {
        super(name);
    }

    @Override
    public void speak() {
        System.out.println("Meow!");
    }
}

class Dog extends Animal {

    public Dog(String name) {
        super(name);
    }

    @Override
    public void speak() {
        System.out.println("Woof!");
    }
}

public class RunTest {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Cat");
        Cat cat2;

        Dog dog1 = new Dog("Dog");
        Dog dog2;

        Animal animal1 = cat1;
        Animal animal2 = dog1;

        if (animal1 instanceof Cat) {
            System.out.println("It's a Cat!");
            cat2 = (Cat)cat1;
            cat2.speak();
        }
        if (animal2 instanceof Dog) {
            System.out.println("It's a Dog!");
            dog2 = (Dog)dog1;
            dog2.speak();
        }
    }
}

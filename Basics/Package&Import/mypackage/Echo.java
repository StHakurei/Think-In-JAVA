//: Basics/Package&Import/mypackage/Echo.java
package mypackage;

/** Example code with usage of package.
 * @author Murica_Reimu
*/
public class Echo {

    public Echo() {
        System.out.println("Echo.");
    }
}

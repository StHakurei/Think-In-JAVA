//: Basics/ShowProperties.java
import java.util.*;

/**
 * This exapmle program is with documentation comments.
 * @author Murica_Reimu
 * @version 1.0
*/
public class ShowProperties {
    /**
     * The static keyword means the particular field or method is not tied to any particular object instance of that class.
     * @param args array of string arguments.
     * @throws exceptions No exceptions thrown.
    */
    public static void main(String[] args) {
        // The code at line 16 will print Java Run Time and JVM information in the terminal.
        System.getProperties().list(System.out);
        // The code at line 18 will print current username(OS) in the terminal.
        System.out.println(System.getProperty("user.name"));
        // The code at line 20 will print the Java library path of current system in the terminal.
        System.out.println(System.getProperty("java.library.path"));
        System.out.println("Hello it's: ");
        // The code at line 23 will output current Central Standard Time (UTC -6) in the terminal.
        System.out.println(new Date());
    }
} /* The last two output: (55% match)
Hello, it's:
Wed Mar 28 11:22:22 CST 2018
*///:~

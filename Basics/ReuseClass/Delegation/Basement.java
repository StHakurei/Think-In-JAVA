//: Basics/ReuseClass/Delegation/Basement.java

/** The third relationship of classes in Java is called "Delegation".
 * Not supported directly by Java.
 * @author Murica_Reimu
*/
public class Basement {
    private String buffer;

    void read(String content) {
        buffer = content;
    }

    String output() {
        return buffer;
    }
}

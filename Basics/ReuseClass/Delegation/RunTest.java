//: Basics/ReuseClass/Delegation/RunTest.java

/** @author Murica_Reimu */
public class RunTest {
    private String key;
    private Basement object = new Basement();

    public RunTest(String key) {
        this.key = key;
    }

    public void store(String input) {
        object.read(input);
    }

    public String withdraw() {
        return object.output();
    }

    public static void main(String[] args) {
        RunTest delegator = new RunTest("Delegator");
        delegator.store("This is a buffered message.");
        System.out.println(delegator.withdraw());
    }
}/* Output: (100% match)
This is a buffered message.
*///:~

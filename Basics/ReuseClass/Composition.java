//: Basics/ReuseClass/Composition.java

/** Resuing classes 1: Composition.
 * @author Murica_Reimu
*/

/** Base class. */
class Fundamental {
    private String msg;

    Fundamental() {
        msg = "Fundamental";
    }

    void showMessage() {
        System.out.println(msg);
    }
}

/** Composed class. */
class Composition {
    Fundamental object;

    Composition() {
        object = new Fundamental();
    }

    void getMessage() {
        object.showMessage();
    }
}

//: Basics/ReuseClass/RunTest.java

/** There is three way to resuing class.
 * Composition, Inheritance, Delegation.
 * The Demo of Delegation in subfolder "Delegation".
 * @author Murica_Reimu
*/
public class RunTest {
    public static void main(String[] args) {
        System.out.println("Composition.");
        Composition object = new Composition();
        object.getMessage();

        System.out.println("Inheritance.");
        Inheritance object_1 = new Inheritance();
        object_1.getMessage();
    }
}/* Output: (100% match)
Composition.
Fundamental
Inheritance.
This is Drivedclass.
*///:~

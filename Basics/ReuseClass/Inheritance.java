//: Basics/ReuseClass/Inheritance.java

/** Resuing classes 2: Inheritance.
 * @Murica_Reimu
*/
class Superclass {
    private String msg;

    Superclass() {
        msg = "This is Superclass.";
    }

    void getMessage() {
        System.out.println(msg);
    }
}

class Inheritance extends Superclass {
    void getMessage() {
        System.out.println("This is Drivedclass.");
    }
}

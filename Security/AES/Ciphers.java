//: Security/AES/Ciphers.java
import java.util.Arrays;
import java.math.BigInteger;
import java.nio.ByteBuffer;
import java.security.SecureRandom;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.InvalidKeyException;
import javax.crypto.spec.GCMParameterSpec;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

/** This tiny program will preforms encrypt and decrypt file with AES. */
public class Ciphers {

    public static void main(String[] args) {
        Ciphers object = new Ciphers();
        String message = "Truth will set you free.";
        String password = "just a key";
        byte[] encryptedData = object.encrypt(password, message);
        BigInteger bi = new BigInteger(1, encryptedData);
        System.out.println("Original Message: " + message);
        System.out.println("Encrypted: " + bi.toString(16));
        System.out.println("Decrypted: " + object.decrypt(password, encryptedData));
    }

    /** Encrypt data with AES algorithm. Use 256 bit length key.
     * @param key - String, encrypt password.
     * @param plainData - String, the data you want to encrypt.
    */
    private byte[] encrypt(String key, String plainData) {
        try {
            /** Use 256 bits length key. */
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] keyBytes = messageDigest.digest(key.getBytes());
            SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");

            /** Generate Initialization Vector (iv). */
            SecureRandom secureRandom = new SecureRandom();
            byte[] iv = new byte[12]; // For GCM is a 12 byte array.
            secureRandom.nextBytes(iv);

            /** Set authentication tag. */
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, parameterSpec);

            /** Encrypt data. */
            byte[] cipherData = cipher.doFinal(plainData.getBytes());

            /** Concat message. */
            ByteBuffer byteBuffer = ByteBuffer.allocate(4 + iv.length + cipherData.length);
            byteBuffer.putInt(iv.length);
            byteBuffer.put(iv);
            byteBuffer.put(cipherData);
            byte[] cipherMessage = byteBuffer.array();
            return cipherMessage;
        }
        catch (NoSuchAlgorithmException exce) {
            exce.printStackTrace();
            return null;
        }
        catch (InvalidKeyException ex) {
            ex.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /** Decrypt data with AES algorithm.
     * @param key - String, password.
     * @param encryptedData - byte array, encrypted data.
    */
    private String decrypt(String key, byte[] encryptedData) {
        try {
            /** Hash password into a 256 bit key. */
            MessageDigest messageDigest = MessageDigest.getInstance("SHA-256");
            byte[] keyBytes = messageDigest.digest(key.getBytes());
            SecretKey secretKey = new SecretKeySpec(keyBytes, "AES");

            /** Deconstruct cipher message. */
            ByteBuffer byteBuffer = ByteBuffer.wrap(encryptedData);
            int ivlength = byteBuffer.getInt();
            byte[] iv = new byte[ivlength];
            byteBuffer.get(iv);
            byte[] cipherData = new byte[byteBuffer.remaining()];
            byteBuffer.get(cipherData);

            /** Initialize cipher. */
            final Cipher cipher = Cipher.getInstance("AES/GCM/NoPadding");
            GCMParameterSpec parameterSpec = new GCMParameterSpec(128, iv);
            cipher.init(Cipher.DECRYPT_MODE, secretKey, parameterSpec);

            /** Decrypt data. */
            byte[] plainData = cipher.doFinal(cipherData);
            String plainText = new String(plainData);
            return plainText;
        }
        catch (NoSuchAlgorithmException exce) {
            exce.printStackTrace();
            return null;
        }
        catch (InvalidKeyException ex) {
            ex.printStackTrace();
            return null;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
}/* Output: (50% match)
Original Message: Truth will set you free.
Encrypted: c5d3828a8e9c2277149ed3afc69001770ef11bff7727476b48691968fdfd3752f6574dccd911992d67e4f2b7a51bb1c03724ef742
Decrypted: Truth will set you free.
*///:~

//: Security/HASH/Verification.java
import java.util.Arrays;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/** This example code will preforms how to verify encrypted password with salt,
 *  Sample password encrypted by algorithm "PBKDF2WithHmacSHA1".
 *  @author Murica_Reimu.
*/
public class Verification {
    public static void main(String[] args) {
        String samplePassword = "1000:a308954730e5f2caaf0579c9499d75f1:c786968aefec1dd7038415a120f9fe3bcaec53f7919d9d14ca90a6530da49b616b599891f26c552df84f4aa3394b87f3f5614cbc50e7ccc1f8834adb713b436e";
        String testPassword0 = "098765tyuiop";
        String testPassword1 = "123456qweasd";

        Verification object = new Verification();
        object.verify(testPassword0, samplePassword);
        object.verify(testPassword1, samplePassword);
    }

    private void verify(String passwd, String encryptedPasswd) {
        try {
            /** Step 1: Separate encrypted password into three parts. */
            String[] parts = encryptedPasswd.split(":");
            int iterations = Integer.parseInt(parts[0]);
            byte[] salt = toByteArray(parts[1]);
            byte[] passwordHash = toByteArray(parts[2]);

            /** Step 2: Try to generate new hash password by test password with original salt and iterations. */
            PBEKeySpec keySpec = new PBEKeySpec(passwd.toCharArray(), salt, iterations, passwordHash.length * 8);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] testHashBytes = keyFactory.generateSecret(keySpec).getEncoded();

            /** Step 3: Try to match test hash with original hash. */
            System.out.println("Original Hash: " + parts[2]);
            System.out.println("Test Hash: " + getHex(testHashBytes));
            if (parts[2].equals(getHex(testHashBytes))) {
                System.out.println("Verified.");
            }
            else {
                System.out.println("Invalid Password.");
            }
        }
        catch (NoSuchAlgorithmException exce) {
            exce.printStackTrace();
        }
        catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String getHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        return bi.toString(16);
    }

    private byte[] toByteArray(String hexString) {
        byte[] hashBytes = new byte[hexString.length() / 2];
        for (int i = 0; i < hashBytes.length; i++) {
            hashBytes[i] = (byte)Integer.parseInt(hexString.substring(i * 2, i * 2 + 2), 16);
        }
        return hashBytes;
    }
}/* Output: (100% match)
Original Hash: c786968aefec1dd7038415a120f9fe3bcaec53f7919d9d14ca90a6530da49b616b599891f26c552df84f4aa3394b87f3f5614cbc50e7ccc1f8834adb713b436e
Test Hash: 6608612faa78392cd10d8b76e9ed97904536f9c3a4da918f0c28974b25f347dcb42fe5e94bd2f980b06858e6493718ee74241dda12fd74e713a45f7b74f212d
Invalid Password.
Original Hash: c786968aefec1dd7038415a120f9fe3bcaec53f7919d9d14ca90a6530da49b616b599891f26c552df84f4aa3394b87f3f5614cbc50e7ccc1f8834adb713b436e
Test Hash: c786968aefec1dd7038415a120f9fe3bcaec53f7919d9d14ca90a6530da49b616b599891f26c552df84f4aa3394b87f3f5614cbc50e7ccc1f8834adb713b436e
Verified.
*///:~

//: Security/HASH/MD5Salt.java
import java.util.Arrays;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/** This piece of example code is for enhancement MD5.
 * @author Murica_Reimu
*/
public class MD5Salt {
    public static void main(String[] args) {
        String password = "123456qweasd";
        try {
            /** Generate salt. */
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG", "SUN");
            byte[] salt = new byte[16];
            secureRandom.nextBytes(salt);

            /** Add salt and hash password. */
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");
            messageDigest.update(salt);
            byte[] hashBytes = messageDigest.digest(password.getBytes());
            System.out.println("Original Password: " + password);
            System.out.println("Encrypted bytes: " + Arrays.toString(hashBytes));

            /** Display encrypt password in Hex format. */
            StringBuilder stringBuilder = new StringBuilder();
            for(int i = 0; i < hashBytes.length; i++){
                stringBuilder.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("MD5(Salt): " + stringBuilder.toString());
        }
        catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}/* Output: (100% match)
Original Password: 123456qweasd
Encrypted bytes: [52, -29, -79, -92, -43, 91, -19, -44, -82, 73, 32, -102, 78, 95, -73, 75]
MD5(Salt): 34e3b1a4d55bedd4ae49209a4e5fb74b
*///:~

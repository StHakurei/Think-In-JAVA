//: Security/HASH/PBKDF2.java
import java.util.Arrays;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;

/** This piece of example code will preforms advanced password encryption
 * using PBKDF2WithHmacSHA1 algorithm.
 * @author Murica_Reimu
*/
public class PBKDF2 {
    public static void main(String[] args) {
        String password = "123456qweasd";
        try {
            /** Generate salt. */
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            secureRandom.nextBytes(salt);

            /** Generate password hash. */
            int iterations = 1000;
            char[] chars = password.toCharArray();
            PBEKeySpec keySpec = new PBEKeySpec(chars, salt, iterations, 64*8);
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
            byte[] hashBytes = keyFactory.generateSecret(keySpec).getEncoded();
            System.out.println("Original Password: " + password);
            System.out.println("Encrypted bytes: " + Arrays.toString(hashBytes));

            /** Display encrypted password. */
            String storedPassword = iterations + ":" + getHex(salt) + ":" + getHex(hashBytes);
            System.out.println("PBKDF2 - " + storedPassword);
        }
        catch (NoSuchAlgorithmException exce) {
            exce.printStackTrace();
        }
        catch (InvalidKeySpecException ex) {
            ex.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String getHex(byte[] array) {
        BigInteger bi = new BigInteger(1, array);
        System.out.println("Big Integer: " + bi);
        String hex = bi.toString(16);
        int paddingLength =(array.length * 2) - hex.length();
        if (paddingLength > 0) {
            System.out.println("Padding called.");
            return String.format("%0" + paddingLength + "d", 0) + hex;
        }
        else {
            return hex;
        }
    }
}/* Output: (100% match)
Original Password: 123456qweasd
Encrypted bytes: [-57, -122, -106, -118, -17, -20, 29, -41, 3, -124, 21, -95, 32, -7, -2, 59, -54, -20, 83, -9, -111, -99, -99, 20, -54, -112, -90, 83, 13, -92, -101, 97, 107, 89, -104, -111, -14, 108, 85, 45, -8, 79, 74, -93, 57, 75, -121, -13, -11, 97, 76, -68, 80, -25, -52, -63, -8, -125, 74, -37, 113, 59, 67, 110]
PBKDF2 - 1000:a308954730e5f2caaf0579c9499d75f1:c786968aefec1dd7038415a120f9fe3bcaec53f7919d9d14ca90a6530da49b616b599891f26c552df84f4aa3394b87f3f5614cbc50e7ccc1f8834adb713b436e
*///:~

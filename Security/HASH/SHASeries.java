//: Security/HASH/SHA1.java
import java.util.Arrays;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/** This piece of example code performs SHA1 encryption.
 * SHA1 - 160 bits hash.
 * @author Murica_Reimu
*/
public class SHASeries {
    public static void main(String[] args) {
        String password = "123456qwezxc";
        try {
            /** Generate salt. */
            SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
            byte[] salt = new byte[16];
            secureRandom.nextBytes(salt);

            /** Add salt and hash password.
             * Switch to 'MessageDigest.getInstance("SHA-256")' will generate 256 bits hash,
             * and it's stronger than SHA1.
             * In addition, the keyword 'MessageDigest' also support argument 'SHA-384' and 'SHA-512'.
            */
            MessageDigest messageDigest = MessageDigest.getInstance("SHA1");
            messageDigest.update(salt);
            byte[] hashBytes = messageDigest.digest(password.getBytes());
            System.out.println("Original Password: " + password);
            System.out.println("Encrypted bytes: " + Arrays.toString(hashBytes));

            /** Display encrypt password in Hex format. */
            BigInteger bi = new BigInteger(1, hashBytes);
            String hex = bi.toString(16);
            System.out.println("SHA1: " + hex);
        }
        catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }
}/* Output: (100% match)
Original Password: 123456qwezxc
Encrypted bytes: [55, 88, 28, -95, 107, 60, -6, -26, 31, 110, -10, 94, -74, -53, 123, 83, 96, -60, 55, -108]
SHA: 37581ca16b3cfae61f6ef65eb6cb7b5360c43794
*///:~

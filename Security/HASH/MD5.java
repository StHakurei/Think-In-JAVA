//: Security/HASH/MD5.java
import java.util.Arrays;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/** This piece of example code will show how to encrypt password to 128 bit hash value.
 * Simple MD5 password encrypt.
 * @author Murica_Reimu
*/
public class MD5 {
    public static void main(String[] args) {
        String password = "123456qweasd";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance("MD5");

            /** Hash password. */
            messageDigest.update(password.getBytes());
            byte[] hashBytes = messageDigest.digest();
            System.out.println("Original Password: " + password);
            System.out.println("Encrypted bytes: " + Arrays.toString(hashBytes));

            /** Display encrypt password in Hex format. */
            StringBuilder stringBuilder = new StringBuilder();
            for(int i=0; i< hashBytes.length ;i++)
            {
                stringBuilder.append(Integer.toString((hashBytes[i] & 0xff) + 0x100, 16).substring(1));
            }
            System.out.println("MD5: " + stringBuilder.toString());
        }
        catch (NoSuchAlgorithmException ex) {
            ex.printStackTrace();
        }
    }
}/* Output: (100% match)
Original Password: 123456qweasd
Encrypted bytes: [-58, -88, 65, -114, 22, 3, 67, 110, -115, 22, -12, 3, -103, -94, -91, -26]
MD5: c6a8418e1603436e8d16f40399a2a5e6
*///:~
